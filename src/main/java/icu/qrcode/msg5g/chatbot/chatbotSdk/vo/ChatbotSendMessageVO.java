package icu.qrcode.msg5g.chatbot.chatbotSdk.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

class PlainTextContent {
    public String text;
    public List<SuggestionUrlAction> suggestions = new ArrayList<>();
}


class SuggestionUrlActionParams {
    public String application = "webview";
    public String viewMode = "full";
    public String url;

    public SuggestionUrlActionParams(String url) {
        this.url = url;
    }
}

class SuggestionUrlAction {
    public String type = "urlAction";
    public String displayText;
    public String postbackData = "set_by_chatbot_reply_yes";
    public SuggestionUrlActionParams actionParams;

    public SuggestionUrlAction(String label, String targetUrl) {
        this.displayText = label;
        this.actionParams = new SuggestionUrlActionParams(targetUrl);
    }
}

public class ChatbotSendMessageVO {
    private String contributionId;
    private String conversationId;
    private String messageType = "text";
    private List<String> destinationAddress = new ArrayList<>();
    private Object content;

    public ChatbotSendMessageVO() {
        this.contributionId = UUID.randomUUID().toString();
        this.conversationId = UUID.randomUUID().toString();
    }

    public ChatbotSendMessageVO addDestination(String phone) {
        this.destinationAddress.add(phone);
        return this;
    }

    public ChatbotSendMessageVO addPlainText(String text) {
        this.messageType = "text";
        PlainTextContent plainText = new PlainTextContent();
        plainText.text = text;
        this.content = plainText;
        return this;
    }

    public ChatbotSendMessageVO addSuggestion(String label, String targetUrl) {
        ((PlainTextContent)this.content).suggestions.add(new SuggestionUrlAction(label, targetUrl));
        return this;
    }
}