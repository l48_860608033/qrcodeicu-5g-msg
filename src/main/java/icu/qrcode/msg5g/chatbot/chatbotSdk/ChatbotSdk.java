package icu.qrcode.msg5g.chatbot.chatbotSdk;

import cn.hutool.http.HttpUtil;
import com.google.gson.Gson;
import icu.qrcode.msg5g.chatbot.chatbotSdk.vo.ChatbotSendMessageVO;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public class ChatbotSdk {
    public String chatbotId = "2687279797@botplatform.rcs.chinaunicom.cn";
    public String appId = "-380e15b11ecbcaba30b6a6ec534";
    public String appKey = "-381e15b11ecbcaba30b6a6ec534";
    public String token = "-382e15b11ecbcaba30b6a6ec534";
    public String oapiUrl = "http://oapi.5g-msg.com:31001";

    private String access_token;

    private String getAccessToken() {
        if (this.access_token != null) {
            return this.access_token;
        }

        String oauthApi = "/walnut/v1/accessToken";
        String timeNonce = UUID.randomUUID().toString();

        try {
            String str = this.appId + this.appKey + timeNonce;
            String signature = getSha256Hex(str).toLowerCase(Locale.ROOT);

            Map<String, String> param = new HashMap<>();
            param.put("appId", appId);
            param.put("nonce", timeNonce);
            param.put("signature", signature);
            String strParam = (new Gson()).toJson(param);
            String result = HttpUtil.createPost(this.oapiUrl + oauthApi)
                    .body(strParam)
                    .execute()
                    .body();

            TTokenResult tokenResult = (new Gson()).fromJson(result, TTokenResult.class);
            this.access_token = tokenResult.data.token;
            return this.access_token;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void sendMessage(String target, String content, String label, String targetUrl) {
        if (this.getAccessToken() == null) {
            return;
        }

        ChatbotSendMessageVO vo = new ChatbotSendMessageVO();
        vo.addDestination(target)
                .addPlainText(content);

        if (label != null) {
            vo.addSuggestion(label, targetUrl);
        }

        String api = "/walnut/v1/sendMessage";
        String str = (new Gson()).toJson(vo);
        String result = HttpUtil.createPost(this.oapiUrl + api)
                .body(str)
                .header("accessToken", this.getAccessToken())
                .execute()
                .body();
    }

    private String getSha256Hex(String text) {
        String shaHex = "";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");

            md.update(text.getBytes("UTF-8"));
            byte[] digest = md.digest();

            shaHex = DatatypeConverter.printHexBinary(digest);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
        }
        return shaHex;
    }
}

class TokenResult {
    public String token;
}

class TTokenResult {
    public String code;
    public String message;
    public TokenResult data;
}