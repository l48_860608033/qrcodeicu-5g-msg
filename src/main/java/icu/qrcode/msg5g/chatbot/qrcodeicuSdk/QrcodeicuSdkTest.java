package icu.qrcode.msg5g.chatbot.qrcodeicuSdk;

import java.util.HashMap;
import java.util.Map;

public class QrcodeicuSdkTest {
    public static void main(String[] args) {

        QrcodeicuSdk sdk = new QrcodeicuSdk();
        sdk.init("https://fxl.ink/portal",
                "https://k8s.qrcode.icu/render",
                "a0205f01a6144bf0984ce832a60e70f9",
                "d80b54922f7c4bb69f0f2ac198359945");

        Map<String, Object> fields = new HashMap<>();
        fields.put("手机号码", "13223232222");
        fields.put("姓名", "张晓萌");

//        Integer count = sdk.useDataTable("21a07495-3a14-482b-9bcb-64ba62217891")
//                .whereEq("手机号码", "1322323222d2")
//                .fetchCount();
        Map<String, Object> record = sdk.useDataTable("ba443876-0a4e-467c-9e6d-ef44bc743cd2")
                .whereEq("手机号码", "18228029017")
                .fetchOne();

//        String recordId = sdk.useDataTable("21a07495-3a14-482b-9bcb-64ba62217891")
//                .createData(fields);
//
//        String url = sdk.useDataTable("21a07495-3a14-482b-9bcb-64ba62217891")
//                .createFormEditUrl(recordId);

        String url = sdk.usePoster("4feff3d1-3c2c-47e0-87b3-38463fe33211", "5b90dc39-e979-4a15-8c5a-dcd6c0e698c7")
                .addTextParam("phone", (String) record.get("手机号码"))
                .addTextParam("name", (String) record.get("姓名"))
                .render();
        int a = 1;
    }
}
