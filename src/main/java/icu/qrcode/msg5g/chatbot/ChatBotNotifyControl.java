package icu.qrcode.msg5g.chatbot;

import icu.qrcode.msg5g.chatbot.chatbotSdk.ChatbotSdk;
import icu.qrcode.msg5g.chatbot.qrcodeicuSdk.QrcodeicuSdk;
import icu.qrcode.msg5g.chatbot.vo.NotifyVO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController()
@RequestMapping("/chatbot/notify")
public class ChatBotNotifyControl {
    @PostMapping("/{chatbotId}/delivery/message")
    public void post(@RequestBody NotifyVO vo,
                     HttpServletRequest request,
                     HttpServletResponse response) throws Exception {
        QrcodeicuSdk qrcodeicuSdk = new QrcodeicuSdk();
        qrcodeicuSdk.init("https://fxl.ink/portal",
                "https://k8s.qrcode.icu/render",
                "a0205f01a6144bf0984ce832a60e70f9",
                "d80b54922f7c4bb69f0f2ac198359945");

        ChatbotSdk chatbotSdk = new ChatbotSdk();

        String dataTableId = "ba443876-0a4e-467c-9e6d-ef44bc743cd2";
        String posterId = "4feff3d1-3c2c-47e0-87b3-38463fe33211";
        String posterCloudStorageId = "5b90dc39-e979-4a15-8c5a-dcd6c0e698c7";

        if ("text".equals(vo.action)) {
            String message = vo.messageData;
            if ("今日海报".equals(message)) {
                Integer count = qrcodeicuSdk.useDataTable(dataTableId)
                        .whereEq("手机号码", vo.sender)
                        .fetchCount();
                if (count == 0) {
                    Map<String, Object> fields = new HashMap<>();
                    fields.put("手机号码", vo.sender);

                    String recordId = qrcodeicuSdk.useDataTable(dataTableId).createData(fields);
                    String url = qrcodeicuSdk.useDataTable(dataTableId).createFormEditUrl(recordId);

                    chatbotSdk.sendMessage(vo.sender, "在使用服务前您需要先完善资料",
                            "点击填写资料", url);
                } else {
                    chatbotSdk.sendMessage(vo.sender, "正在处理中，请稍等5秒", null, null);

                    Map<String, Object> record = qrcodeicuSdk.useDataTable(dataTableId)
                            .whereEq("手机号码", vo.sender)
                            .fetchOne();
                    String url = qrcodeicuSdk.usePoster(posterId, posterCloudStorageId)
                            .addTextParam("phone", (String) record.get("手机号码"))
                            .addTextParam("name", (String) record.get("姓名"))
                            .render();

                    chatbotSdk.sendMessage(vo.sender, "您的海报已生成",
                            "点击领取", url);
                }
            } else if ("完善资料".equals(message)) {
                Integer count = qrcodeicuSdk.useDataTable(dataTableId)
                        .whereEq("手机号码", vo.sender)
                        .fetchCount();
                if (count > 0) {
                    Map<String, Object> record = qrcodeicuSdk.useDataTable(dataTableId)
                            .whereEq("手机号码", vo.sender)
                            .fetchOne();
                    String url = qrcodeicuSdk.useDataTable(dataTableId).createFormEditUrl((String) record.get("recordId"));
                    chatbotSdk.sendMessage(vo.sender, "请点击完善资料",
                            "点击填写", url);
                }
            }
        }
    }
}
