package icu.qrcode.msg5g.chatbot.vo;

public class NotifyVO {
    public String contributionId;
    public String sender;
    public String conversationId;
    public String action;
    public String messageId;
    public String messageData;
}