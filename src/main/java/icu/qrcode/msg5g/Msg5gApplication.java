package icu.qrcode.msg5g;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Msg5gApplication {

    public static void main(String[] args) {
        SpringApplication.run(Msg5gApplication.class, args);
    }

}
